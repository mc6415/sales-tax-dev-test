# Sales Tax Dev Test

This is the sales tax dev test, it has been written as a simple React application using create-react-app to get 
most of the boilerplating done.

To run checkout the project, go into the directory and use:

```npm install```

And then

```npm run start```

It should then load up on localhost.