import React from 'react';
import './App.css';
import ItemsForm from "./Components/ItemsForm";

function App() {
  return (
    <div className="App">
      <ItemsForm />
    </div>
  );
}

export default App;
