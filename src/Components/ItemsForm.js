import React, {Component} from 'react';
import {Button, Checkbox, Container, Control, Icon, Input, Table} from 'bloomer';
import Receipt from './Receipt';

export default class ItemsForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
        }
    }

    addItem = () => {
        let {items} = this.state;

        items.push({
            itemName: '',
            cost: 0.00,
            salesTax: false,
            imported: false,
            afterTax: 0.00,
        });

        this.setState(
            {
                items,
            }
        )
    }

    updateItem = (index, e) => {
        let {items} = this.state;
        let item = items[index];
        item[e.target.name] = e.target.value;
        items[index] = item;

        this.setState({
            items,
        }, () => this.calculate())
    }

    updateCheckbox = (index, e) => {
        let {items} = this.state;
        let item = items[index];

        item[e.target.name] = !item[e.target.name];
        items[index] = item;

        this.setState({
            items,
        }, () => this.calculate())
    }

    removeItem = (index) => {
        let {items} = this.state;

        items.splice(index, 1);

        this.setState({
            items,
        })
    }

    calculate = () => {
        // This should go through and calculate the total after tax for each item.
        let {items} = this.state;

        items.forEach((item) => {
            item.afterTax = this.valueAfterTax(item);
        });

        this.setState({
            items,
        })
    }

    valueAfterTax = (item) => {
        // If no tax is due just return the original cost
        if (!item.salesTax && !item.imported) {
            return item.cost;
        }

        // Figure out the total tax rate
        let totalTax = 1.00;
        totalTax = item.salesTax ? totalTax += 0.1 : totalTax;
        totalTax = item.imported ? totalTax += 0.05 : totalTax;

        let afterTax = (item.cost * totalTax);

        // I believe this should round up to the nearest 0.05 if any tax is applied.
        // This is done on the whole amount!
        return afterTax.toFixed(2);
    }

    render() {
        let {items} = this.state;

        // TODO: Add quantity functionality but seeing as no examples in the example call for more than 1 of an item skipping for now.

        return (
            <Container>
                <Table>
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Cost</th>
                        <th>Sales Tax Applied</th>
                        <th>Imported</th>
                        <th>After Tax</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items.length > 0 && items.map((item, index) => (
                        <tr key={`item-${index}`}>
                            <td>
                                <Control>
                                    <Input name='itemName' value={item.itemName}
                                           onChange={(e) => this.updateItem(index, e)}/>
                                </Control>
                            </td>
                            <td>
                                <Control hasIcons>
                                    <Input name='cost' type='number' step='0.01' value={item.cost}
                                           onChange={(e) => this.updateItem(index, e)}/>
                                    <Icon className='fa fa-pound-sign' isSize='small' isAlign='left'/>
                                </Control>
                            </td>
                            <td>
                                <Checkbox name='salesTax' checked={item.salesTax}
                                          onChange={(e) => this.updateCheckbox(index, e)}/>
                            </td>
                            <td>
                                <Checkbox name='imported' checked={item.imported}
                                          onChange={(e) => this.updateCheckbox(index, e)}/>
                            </td>
                            <td>
                                <Control hasIcons>
                                    {/* No need for the onChange here as this is read only */}
                                    <Input name='afterTax' type='number' step='0.01' value={item.afterTax}/>
                                    <Icon className='fa fa-pound-sign' isSize='small' isAlign='left'/>
                                </Control>
                            </td>
                            <td>
                                {/* In case of accidental items being added */}
                                <Button onClick={() => this.removeItem(index)}>Remove Item</Button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
                <Button onClick={this.addItem}>Add Item</Button>
                <br/>
                <Receipt items={items}/>
            </Container>
        )
    }
}