import React, {Component} from 'react';
import {Container} from 'bloomer';

export default class Receipt extends Component {
    constructor(props) {
        super(props);
    }

    totalTax = () => {
        let {items} = this.props;

        // Using the + before the variables in order to ensure they're cast to a float
        let totalBeforeTax = items.reduce((a, b) => +a + +b.cost, 0);
        let totalAfterTax = items.reduce((a, b) => +a + +b.afterTax, 0);

        let totalTaxes = totalAfterTax - totalBeforeTax;

        return (Math.ceil(totalTaxes * 20) / 20).toFixed(2);
    }

    render() {
        let {items} = this.props;

        let totalCost = items.reduce((a, b) => +a + +b.cost, 0).toFixed(2);

        console.log(totalCost);

        return (
            <Container>
                {items && items.map(item => (
                    <span>1 {item.itemName}: £{item.afterTax}<br/></span>
                ))}
                <br/>
                Sales Taxes: £{this.totalTax()} <br/>
                Total: £{+totalCost + +this.totalTax()}
            </Container>
        )
    }
}
